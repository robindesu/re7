// Account details definition
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // 8883 for MQTTS
#define AIO_USERNAME "joshuasardinha"
#define AIO_KEY "0d716abb6d424e258688668d311d6002"

#define GREEN_LED_PIN D0
#define YELLOW_LED_PIN D1
#define BUTTON_PIN D2

// Wi-Fi definitions
#define WIFI_SSID "Josh"
#define WIFI_PASS "thorDogao"

#include "AdafruitIO_WiFi.h"
// Set up the Wi-Fi connection

const char MQTT_SERVER[] = AIO_SERVER;
const char MQTT_USERNAME[] = AIO_USERNAME;
const char MQTT_PASSWORD[] = AIO_KEY;

const char IS_LOCKED_FEED[] = AIO_USERNAME "/feeds/is-locked";

WiFiClient client;

// Set up the subscription
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

// Set up the 'isLocked' feed
Adafruit_MQTT_Subscribe isLockedSubscribe = Adafruit_MQTT_Subscribe(&mqtt, IS_LOCKED_FEED);
Adafruit_MQTT_Publish isLockedPublish = Adafruit_MQTT_Publish(&mqtt, IS_LOCKED_FEED);

int currentState = 0;
int counter = 0;
int oscilatorState = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  while(!Serial);

  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(YELLOW_LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(GREEN_LED_PIN, LOW);
  digitalWrite(GREEN_LED_PIN, HIGH);
  // Connect to WiFi access point
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  
  Serial.println("WiFi Connected");
  Serial.print("IP Address ");
  Serial.println(WiFi.localIP());

  mqtt.subscribe(&isLockedSubscribe);
}

// the loop function runs over and over again forever
void loop() {
  MQTT_connect();

  Adafruit_MQTT_Subscribe *subscription;
  if (currentState == 0) {
    while (subscription = mqtt.readSubscription(1000)) {
      if (subscription == &isLockedSubscribe) {
        Serial.print(F("Got: "));
        Serial.println((char *)isLockedSubscribe.lastread);
        char *lastRead = (char *)isLockedSubscribe.lastread;
        int feedValue = *lastRead - 48;
        Serial.print(F("Int Value: "));
        Serial.println(feedValue);
        if (feedValue == 1 && currentState == 0) {
          Serial.println("Ready to receive.");
          currentState = feedValue;
          digitalWrite(GREEN_LED_PIN, HIGH);
        }
      }
    }
  } else if (currentState == 1){
    counter++;
    if (counter % 300000 == 0) {
      Serial.print(".");
    }
    if (digitalRead(BUTTON_PIN) == HIGH) {
      Serial.println("Garbage Collected");
      digitalWrite(YELLOW_LED_PIN, HIGH);
      counter = 0;
      currentState = 2;
      isLockedPublish.publish(2);
    }
    else if (counter > 3000000) {
      Serial.println("Timeout");
      digitalWrite(GREEN_LED_PIN, LOW);
      isLockedPublish.publish(0);
      currentState = 0;
      counter = 0;
    }
  } else {
    counter++;
    if (counter > 1000000) {
      Serial.println("Rewards achieved!");
      digitalWrite(GREEN_LED_PIN, LOW);
      digitalWrite(YELLOW_LED_PIN, LOW);
      isLockedPublish.publish(0);
      currentState = 0;
      counter = 0;
    }
   
  }
}

void MQTT_connect() {
  int8_t ret;

  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");
  while ((ret = mqtt.connect()) != 0) {
    //Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();
    delay(5000);
  }
  Serial.println("MQTT Connected!");
}
