# Re7

The Re7 Project was designed to help and motivate common citizens to handle their waste properly. Its main purpose is to display waste containers which content will be taken to recycling and which location is known by the Re7 system users. The users can use the Re7 app to be informed about the containers locations and to search for rewards whenever their waste is deposited.

The system mostly consists of 3 parts: an application, developed within the Ionic framework; the containers, controlled by an Arduino board; and the MQTT server, responsible to maintain the container state feed, to which the app and the container will comunicate proactively.

## Getting Started

To test and use the system, you must have access to all the 3 parts: the Ionic framework, the Arduino Board and the MQTT Adafruit IO server.

### Prerequisites

#### Arduino

To setup the Arduino board, the Arduino IDE and the ESP8266 board are necessary.

* [Arduino IDE](https://www.arduino.cc/en/Main/Software) - The IDE to be installed
* [Node MCU](https://www.nodemcu.com/index_en.html) - The ESP8266 Board Used

#### Ionic
To run in the browser can be used:
Ionic serve
or 
Ionic cordova run browser

#### MQTT Server

To access, publish and subscribe to the feed state using both container and application, it is necessary to have an account in the Adafruit IO:

* [Adafruit IO](https://io.adafruit.com/)

With an account, it is possible to setup a feed to each container.

### Installing

After creating an account, it is necessary to implement the AIO_USERNAME and AIO_KEY constants in the Arduino main code, named container.ino. It is also necessary to change the WIFI_SSID and the WIFI_PASS constants to match the local WiFi network. After creating a feed in such account, also change the IS_LOCKED_FEED variable to: AIO_USERNAME "/feeds/feedname". If the board can connect to the network and to the Adafruit IO, the container should work fine.

## Authors

* **Joshua Sardinha**
* **Carlos Robson de Oliveira**
* **Celso Mizerani**
* **Arthur Maia Mendes**

## License

This project is a project developed within licensed within the Unicamp - Campinas State University.

## Acknowledgments

* The project is still under development 

