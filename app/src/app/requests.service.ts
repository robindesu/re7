import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' ,  'X-AIO-Key': '0d716abb6d424e258688668d311d6002'})
};
const dataAddrs: string =  "https://io.adafruit.com/api/v2/joshuasardinha/feeds/is-locked/data"
const feedsAddrs: string = 'https://io.adafruit.com/api/v2/joshuasardinha/feeds ';

@Injectable ()
export class RequestsService {

  constructor(private http:HttpClient) {}
  bodyObj = {
    'feed': {
      "description": "",
      "key": "is-locked",
      "license": null,
      "name": "is-locked",
    }
  };
  // Uses http.get() to load data from a single API endpoint
  getData() {
    return this.http.get(feedsAddrs, httpOptions);
  }
  updateData(value) {
    return this.http.post(dataAddrs,
      {
        "value": 1
      }, httpOptions)
  }
}
