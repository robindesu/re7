import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the LocationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-locations',
  templateUrl: 'locations.html',
})
export class LocationsPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationsPage');
    this.initMap();
  }

  // Initialize and add the map
  initMap() {
  let map = new google.maps.Map(
      document.getElementById('map'), {zoom: 18, center: {lat: 0, lng: 0}});

    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      map.setCenter(pos);
    });

    let salvador = {lat: -12.8750996, lng: -38.641738};
    let manaus = {lat: -3.0443101, lng: -60.1071915};

    let marker1 = new google.maps.Marker({position: salvador, map: map});
    let marker2 = new google.maps.Marker({position: manaus, map: map});
  }
}
