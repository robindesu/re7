import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RequestsService } from '../../app/requests.service';

/**
 * Generated class for the CollectConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-collect-confirm',
  templateUrl: 'collect-confirm.html',
})
export class CollectConfirmPage {
  @Output() emitPoints = new EventEmitter;
  isLoading = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,  private request: RequestsService) {
    console.log(navParams.get('id'));
    this.openContainer();
  }

   openContainer() {
    this.request.updateData(1).subscribe(data => {
      console.log(data);
      this.emitPoints.emit({"points": 10});
      setTimeout(() => {
        this.isLoading = false
        this.request.updateData(0);
      }, 1000)
    });
  }

}
